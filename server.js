const app = require('express')();
const http = require('http').Server(app);
const Redis = require("ioredis");
const redisClient = new Redis(6379,'10.0.100.131');
const io = require('socket.io')(http,{
    cors: {
      origin: "http://localhost:4200",
      methods: ["GET", "POST"]
    },
    /*adapter: require("socket.io-redis")({
     pubClient: redisClient,
     subClient: redisClient.duplicate(),
    }),*/
  });

const { RedisSessionStore } = require("./sessionStore");
const sessionStore = new RedisSessionStore(redisClient);

const { RedisMessageStore } = require("./messageStore");
const messageStore = new RedisMessageStore(redisClient);

  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });


  io.use(async (socket, next) => {
    const sessionID = socket.handshake.auth.sessionID;
    console.log(socket.handshake)
    next();
    /*if (sessionID) {
      const session = await sessionStore.findSession(sessionID);
      if (session) {
        socket.sessionID = sessionID;
        socket.userID = session.userID;
        socket.username = session.username;
        return next();
      }
    }
    const username = socket.handshake.auth.username;
    if (!username) {
      return next(new Error("invalid username"));
    }
    socket.sessionID = randomId();
    socket.userID = randomId();
    socket.username = username;
    next();*/
  });
  
io.on('connection', function(socket) {
    console.log('A user connected');
 
    //Whenever someone disconnects this piece of code executed
    socket.on('disconnect', function () {
       console.log('A user disconnected');
    });
 });

http.listen(3001, function() {
   console.log('listening on *:3001');
});